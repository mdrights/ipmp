FROM node:10-buster as builder
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY . /usr/src/app/
RUN npm install && npm run clean
ENV GATSBY_PREFIX /
ENV IPFS_PATH /srv/go-ipfs/ipfsrepo
RUN ./node_modules/.bin/gatsby build --prefix-paths

FROM socialengine/nginx-spa:latest
COPY --from=builder /usr/src/app/public/ /app
RUN chmod -R 755 /app
