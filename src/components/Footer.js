import React from 'react'
import { Link } from 'gatsby'

// import facebook from '../img/social/facebook.svg'
// import twitter from '../img/social/twitter.svg'
import ccIcon from '../img/cc_icon_white_x2.png'
import byIcon from '../img/attribution_icon_white_x2.png'
import ncIcon from '../img/nc_white_x2.png'
import ndIcon from '../img/nd_white_x2.png'
import '../styles/Footer.sass'

const Footer = () => {
  return (
    <footer className="footer sitefooter">
      <div className="sitefooter__logo">
        <Link to="/about/" style={{color: 'white'}}>
          关于我们
        </Link>
      </div>
      <div className="sitefooter__cc">
        <a
          rel="license noopener noreferrer"
          target="_blank"
          className="dim"
          href="https://creativecommons.org/licenses/by-nc-nd/4.0/"
        >
          <div className="sitefooter__cclogo">
            <img src={ccIcon} alt="CC" />
            <img src={byIcon} alt="BY" />
            <img src={ncIcon} alt="NC" />
            <img src={ndIcon} alt="ND" />
          </div>
          若無特別註明，本站內容采用知识共享署名-非商业性使用-禁止演绎 4.0 国际许可协议进行许可。
        </a>
      </div>
    </footer>
  )
}

export default Footer
