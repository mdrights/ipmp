import { graphql } from 'gatsby'

export const fullColumnistFields = graphql`
  fragment fullColumnistFields on MarkdownRemark {
    meta: frontmatter {
      title: columnist_title
      order
      cover {
        childImageSharp {
          fluid(maxWidth: 400, quality: 80) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  }
`

export const simpleColumnistFields = graphql`
  fragment simpleColumnistFields on MarkdownRemark {
    fields {
      slug
    }
    meta: frontmatter {
      title: columnist_title
    }
  }
`

export const articleThumbFields = graphql`
  fragment articleThumbFields on MarkdownRemark {
    fields {
      slug
    }
    excerpt(pruneLength: 60, truncate: true)
    id
    frontmatter {
      title,
      date(formatString: "YYYY/MM/DD")
      columnist {
        ...simpleColumnistFields
      }
      featuredimage {
        childImageSharp {
          fluid(maxWidth: 400, quality: 80) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  }
`