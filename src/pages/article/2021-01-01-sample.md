---
templateKey: article
title: Sample
slug: this-is-sample
date: 2018-11-02T07:29:12.668Z
description: This is a nice sample
contributors:
  - type: 作者
    name: xxx
  - type: 校对
    name: xxx
featuredpost: true
featuredimage: /img/Gatsbyjs-love.jpg
trending: 1
isbrief: false
columnist: default
tags:
  - abc
  - '1234'
---

# IPMP: 分布式独立媒体内容发布平台  

本网站基于 GatsbyJS 框架 + Netlify CMS 编辑台。  

下面简要介绍文章的格式要求。  

## 文件名：

MD 格式文件要求文件名统一为：`年-月-日-文章标题.md`

文件名中的文章标题可以带空格，但为了操作方便最好不要带空格。
年-月-日：指8位數字：YYYY-MM-DD。可以是该文章发表的日期。不过在文件内部还可以设定发表日期（在 frontmatter ），最终网页上显示的日期和时间会以文件文件内部的为准。

每篇文章一個 MD 文件，每個專欄作家也需要設立一個 MD 文件。 


## Frontmatter

就是每个md 文件内开头的两行 --- 之间的内容。可以给文件定义很多属性。其中文章属于哪个类别、专栏啦、tag 啦，都在这里指定就好。
具体有哪些属性呢？可以瞅瞅我们现有网页在代码库是什么样子就知道了。  

具体要求是这样的：

```
templateKey: article （网页模板，一般为 article）
title: （这里如填写了那就会覆盖该文件文件名上的标题）
slug:  （这里的英文单词将作为这篇文章的网址的一部分，方便记忆）  
date: 2018-11-02T07:29:12.668Z（当作发表日期时间，随意指定）
description: （文章的摘要，显示在文章列表等位置）
contributors:
  - type: 作者
    name: xxx
  - type: 校对
    name: xxx
featuredpost: true or false （是否进入轮播）
featuredimage: /img/xxxxx.jpg （轮播时用的图，注意这里时图片文件名及前缀 /img/）
trending: （热度推荐排名；热度 0 ~ 10 的文章，最多顯示 10 篇，數字越大越前面，同分時，刊登日期越大越前面）
isbrief: true or false （是否是早报；注意早报也是一个专栏，所以早报和专栏只能选一个）
columnist: （专栏作家名字。注意：需要爲每個專欄作家設立也是 MD 格式的個人介紹的文件）
tags:  （标签，如下，数字的话用单引号）
  - 勞工
  - '2020'
```


## 关于 markdown 语法


Markdown 语法有多种，我们统一用 GitHub 那种标准。

由于文章大多从 doc 和 html 转换而来，有些文字格式可能会丢失（转换工具未必很智能），有的格式 md 不支持（如字体颜色）—— 因此需要最后人工修订和校对。

优先关注文章的标题、加黑、斜体等等，字体颜色和正文字体大小先不关注。

如果文章内有表格，建议直接用其截图比较方便。markdown 虽然支持表格，带略为复杂，不过仍然可以生成（有这些在线工具可以帮你生成 md 表格）。

关于图片：md 要求图片单独于文章成独立文件，全部放在一个文件夹里（位置： static/img/）。文件名不做要求，但文件名不重复就好。然后在文章里出现图片的地方用这样的语法指定图片的位置就好：

一般图片：
```
![](/img/picture-1.png "本图片的一句注释" )
```

> 注意：  
> 1. 图片文件名和后缀由你们来定义，每个图片文件名不同即可。
> 2. 那双引号是英文半角的。

所以要记住哪个图片是出现在文章的哪个地方哟亲！




最后建议：网站天然使用 md 格式的文章，请写作时直接用 markdown 写就好。

Happy writing!
