# IPMP - InterPlanetary Media Platform

IPMP 是一個旨在盡可能在 server side 實施抗審查技術的、爲新聞媒體機構設計優化的**網站/媒體內容管理系統**。  



## 基本介紹

- 當前設計運行的技術有：
	- 全站基於 [Git](https://git-scm.com/) —— 分布式的代碼+內容 版本管理技術，開發者和內容作者均可通過 Git 分布式地存儲、創作、搭建和發佈網站和內容（代碼和內容放在同一個 repository 內）； 
	- 同時運行在常規（web）網絡和 [IPFS](https://ipfs.io/) 「星際」網絡上；  
	- 運行在 [Tor Onion](https://community.torproject.org/onion-services/) 暗網網絡上；  
	- 設有 [OPENNIC](https://www.opennic.org/) 域名；  
	- 設有 [ETH/ENS](https://ens.domains/) 域名。  

- 通過以上方法均可以（**在牆內**）訪問到網站內容，我們在精力範圍內盡己所能確保以上技術的有效。具體訪問方法請參見[這篇 WiKi](https://gitlab.com/bmdvY24K/ipmp/-/wikis/Manuals/%E8%AE%BF%E9%97%AE%E7%BD%91%E7%AB%99%E7%9A%84%E5%90%84%E7%A7%8D%E5%A7%BF%E5%8A%BF)。  

- 每種技術都附帶其官方鏈接，可點擊了解更多。  

- 歡迎建議或貢獻更多抗審查（服務端）的技術！
	- 參與方式包括：
    - 在本代碼倉庫提問（使用 issue；請閱讀[issue 處理流程](https://gitlab.com/bmdvY24K/ipmp/-/wikis/Manuals/issues%E8%99%95%E7%90%86%E6%B5%81%E7%A8%8B%E8%A6%8F%E5%89%87)）；  
    - 代碼捉蟲/新功能建議（提交 Merge Request）；  
    - 發郵件給網站管理員 wahaha at dismail dot de  


## 架構簡介

- 目前網站在物理上包含：
	- 1臺常規 web server  
	- 1臺 IPFS daemon server  
	- 2臺 IPFS gateway server  
	- 1臺 CMS server（由 Gitlab.com 免費託管）  
	- N臺 Onion Service server   

- 網站邏輯架構圖：  

![logic arch](https://user-content.gitlab-static.net/f25c274aea5a6d3f7c7520eb35300d9fc7ac18aa/68747470733a2f2f692e696d6775722e636f6d2f4f5a716e7633502e706e67)  

- 工作/數據流圖：

![workflow](https://i.imgur.com/hvkhO31.png)  

- 本站選用以上技術的原因在[這篇 WiKi](https://gitlab.com/bmdvY24K/ipmp/-/wikis/Manuals/%E5%9F%BA%E6%9C%AC%E6%9E%B6%E6%A7%8B%E5%92%8C%E5%B7%A5%E4%BD%9C%E6%B5%81)。  

<hr />

## 編輯者、作者使用資訊

IPMP 由以下類型的內容組成：

1. 文章：一篇文章即單篇的報導，亦提供後設欄位，設定諸如標籤、專題等資訊
2. 輪播清單：由一組文章組成的輪播清單，會顯示在首頁上
3. 熱門文章清單：由一組文章組成的輪播清單，會顯示在首頁的側邊欄
4. 首頁：顯示手動選擇的文章，以及根據日期最新發佈文章

## 網站設定

- `gatsby-config.js`
  - logo: 網站 Logo 檔名，注意檔案須放在 `static/img` 下
  - title: 網站名稱，將會顯示在瀏覽器標題列
  - description: 網站說明，將會是社群平台、搜尋引擎顯示的文字
  - gatewayCid / gatewayAnswer: 測試分佈式入口的 CID 與答案
  - ipns: 網站的 IPNS
  - mainSite: 主網站的網域，會用在社群分享時的封面圖片連結中

### 文章 (Article)

1. 後設資料：
   - description: 選填，分享至社群平台時會顯示的文字
   - isbrief: 選填，沒填時預設為 `false` 。 當設為 `true` 時，文章會顯示在 `早報` 頁面中。
   - columnist: 選填，當有填寫時，會被歸類在 `專欄` 中
   - featuredpost: 選填，沒填時預設為 `false` 。 當設為 `true` 時，文章會顯示在首頁輪播。
   - trending: 選填，沒填時預設為 `false` 。 當設為 `true` 時，文章會顯示在熱度推薦中。
   - contributors: 選填，作者、編輯、攝影等等人員

- 更詳盡的編輯工作指引，在[這篇 WiKi](https://gitlab.com/bmdvY24K/ipmp/-/wikis/Manuals/%E7%B6%B2%E7%AB%99%E7%B7%A8%E8%BC%AF%E8%AA%AA%E6%98%8E)。  

<hr />

## 網站搭建和部署資訊

### 前端技術

0. [Gatsby](https://gatsbyjs.org) 靜態框架  
1. CMS: [Netlify CMS Starter](https://github.com/netlify-templates/gatsby-starter-netlify-cms)
2. CSS library

### 系統需求

1. 建議使用 Node 10+ LTS
2. 一個支援 [Netlify CMS backend](https://www.netlifycms.org/docs/authentication-backends/) 的 Git  
3. GNU/Linux server（RAM >= 3G）；如需部署 IPFS 則建議至少 2 臺server，分開部署    
4. IPFS (Go) >= 0.5.0  
5. Tor >= 0.4.x
6. 大部分系統層面部署任務都可 CI/CD 自動化，參見本 repo 的 [.gitlab-ci.yml](https://gitlab.com/bmdvY24K/ipmp/-/blob/feature/.gitlab-ci.yml)  
6. 部分 server 部署及運維腳本在：[Scripts](https://gitlab.com/bmdvY24K/scripts)

### 安裝步驟

`npm i` or `yarn i`

### 開發指令

```bash
npm run develop
```

### 發佈指令

```bash
# 常規 web 網站  
npm run build  

# IPFS + Nginx 反向代理  
env GATSBY_PREFIX=/ipfs/<CID> npm run build:ipfs
```

最後靜態文件均存放在 `public` 資料夾。  

